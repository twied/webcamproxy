# What's this?

![screenshot](screenshot.png)

WebcamProxy lets users intercept their webcam's video stream and perform basic
image manipulation operations, such as freezing an image, overlaying an image
or replacing the webcam's stream with a static image.

The idea is to be able to display a filler picture when having to leave a video
conference for a minute, e.g. to grab a drink, without having to announce the
absence or having other participants wonder where the user went.

# Build and installation

WebcamProxy uses the [meson](https://mesonbuild.com/) build system and requires
[Qt5](https://www.qt.io/) and
[v4l2loopback](https://github.com/umlaeute/v4l2loopback) for operation.

Compile the gui:

```{sh}
$ apt-get install meson ninja qtbase5-dev v4l2loopback-dkms
$ meson build && ninja -C build

# optional:
$ ninja -C build install
```

Load the kernel module:

```{sh}
$ sudo modprobe v4l2loopback exclusive_caps=1

# or, to make changes permanent:
$ echo "v4l2loopback" > /etc/modules-load.d/v4l2loopback.conf
$ echo "options v4l2loopback exclusive_caps=1 video_nr=10" > /etc/modprobe.d/v4l2loopback.conf
$ update-initramfs -u
```

# Configuration

Store pictures in `${HOME}/.config/webcamproxy/images/`. Pictures with
transparency are automatically recognized as overlays, pictures without
transparency will replace the camera video stream when selected.

Select the input video device with `webcamproxy --input /path/to/device` and
the output video device with `webcamproxy --output /path/to/device`. Resonable
defaults should be selected automatically, but can be altered in
`${HOME}/.config/webcamproxy.conf`:

```{ini}
[General]
input=/path/to/device
output=/path/to/device
```

Run `webcamproxy --help` to see all command line options.

# Run

1. Load the kernel module and start WebcamProxy before you start your video
capturing application, video conference software, browser tab, etc.
2. Start the conference software and select the virtual web cam as video device
to use.
3. Use the WebcamProxy gui to manipulate your video stream.
4. ...
5. Profit!

# License

All code is placed unter the terms of the
[General Public License v2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
or later.

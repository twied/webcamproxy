// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef CANVAS_H_
#define CANVAS_H_

#include <string>
#include <vector>

class Canvas {
public:
    Canvas(const std::string& input, const std::string& output);
    Canvas(const Canvas&) = delete;
    Canvas(Canvas&&) = delete;
    Canvas& operator=(const Canvas&) = delete;
    Canvas& operator=(Canvas&&) = delete;
    ~Canvas();

    auto& data() {
        return m_data;
    }

    int width() const {
        return m_width;
    }

    int height() const {
        return m_height;
    }

    void read_image();
    void write_image();

    void enable_input(bool value);

private:
    std::vector<unsigned char> m_data {};
    int m_input;
    int m_output;
    unsigned int m_width;
    unsigned int m_height;
    bool m_enable_input { true };
};

#endif /* CANVAS_H_ */

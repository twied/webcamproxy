// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "canvas.h"
#include "gui.h"
#include "imageops.h"

#include <thread>

int main(int argc, char* argv[]) {
    Gui gui { argc, argv };
    Canvas canvas { gui.video_input(), gui.video_output() };

    std::thread thread {
        [&]() {
            while (!gui.terminated()) {
                ImgOps* imgop = gui.current_op();

                canvas.enable_input(imgop->enableCamera());
                imgop->fill(canvas);
                canvas.write_image();
            }
        }
    };

    auto retval = gui.run();
    thread.join();

    return retval;
}

// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef IMAGEOPS_H_
#define IMAGEOPS_H_

#include <string>

class Canvas;

class ImgOps {
public:
    virtual ~ImgOps() noexcept = default;
    virtual void fill(class Canvas& canvas) = 0;
    virtual std::string name() = 0;
    virtual std::string symbol() = 0;
    virtual bool enableCamera() {
        return true;
    }
};

class OpPassthrough : public ImgOps {
public:
    void fill(class Canvas& canvas) override;
    std::string name() override {
        return "Passthrough";
    }
    std::string symbol() override {
        return "🎬";
    }
};

class OpHoldImage : public ImgOps {
public:
    void fill(class Canvas& canvas) override;
    std::string name() override {
        return "Hold Image";
    }
    std::string symbol() override {
        return "⏸️";
    }
    bool enableCamera() override {
        return false;
    }
};

class OpSolidColor : public ImgOps {
public:
    OpSolidColor(unsigned char r, unsigned char g, unsigned char b);
    void getColor(unsigned char& r, unsigned char& g, unsigned char& b);
    void setColor(unsigned char r, unsigned char g, unsigned char b);
    void fill(class Canvas& canvas) override;
    std::string name() override {
        return "Solid Color";
    }
    std::string symbol() override {
        return "◆";
    }
    bool enableCamera() override {
        return false;
    }

private:
    unsigned char m_red;
    unsigned char m_green;
    unsigned char m_blue;
};

class OpStaticImage : public ImgOps {
public:
    OpStaticImage(const std::string& filename);
    void fill(class Canvas& canvas) override;
    std::string name() override {
        return m_name;
    }
    std::string symbol() override {
        return m_overlay ? "🎥" : "🎨";
    }
    bool enableCamera() override {
        return m_overlay;
    }

private:
    void* m_impl {};
    bool m_overlay {};
    std::string m_name;
};

#endif /* IMAGEOPS_H_ */

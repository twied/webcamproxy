// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef GUI_H_
#define GUI_H_

#include <functional>
#include <string>
#include <vector>

class ImgOps;
class QApplication;
class QLabel;
class QLayout;
class QListWidget;
class QWidget;

class Entry {
public:
    Entry(ImgOps*);

    void add_button(const char* name, std::function<void(void)> click);

    ImgOps* ops() const {
        return m_ops;
    }
    QWidget* widget() const {
        return m_widget;
    }
    QLabel* symbol() const {
        return m_symbol;
    }
    QLabel* name() const {
        return m_name;
    }

private:
    ImgOps* m_ops;
    QLayout* m_layout;
    QWidget* m_widget;
    QLabel* m_symbol;
    QLabel* m_name;
};

class Gui {
public:
    Gui(int& argc, char** argv);
    ~Gui();

    std::string video_input() {
        return m_video_input;
    }

    std::string video_output() {
        return m_video_output;
    }

    bool terminated();
    ImgOps* current_op();
    int run();

private:
    std::string m_video_input {};
    std::string m_video_output {};
    bool m_terminated {};
    ImgOps* m_current_op {};
    std::vector<ImgOps*> m_imgops {};
    QApplication* m_application {};
    QListWidget* m_widget {};
};

#endif /* GUI_H_ */

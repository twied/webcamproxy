// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "gui.h"

#include "imageops.h"

#include <QApplication>
#include <QColorDialog>
#include <QCommandLineParser>
#include <QDir>
#include <QHBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QSettings>
#include <QStandardPaths>

#include <mutex>

static std::mutex g_mutex {};
constexpr static const char* DEFAULT_VIDEO_INPUT = "/dev/video0";
constexpr static const char* DEFAULT_VIDEO_OUTPUT = "/dev/video1";

static void default_devices(std::string& input, std::string& output) {
    QSettings settings { "webcamproxy" };
    if (input.empty()) {
        input = settings.value("input").toString().toStdString();
    }

    if (output.empty()) {
        output = settings.value("output").toString().toStdString();
    }

    QDir dir { "/sys/class/video4linux" };
    if (dir.exists() && (input.empty() || output.empty())) {
        dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

        for (const auto& finfo : dir.entryInfoList()) {
            auto name = finfo.fileName().toStdString();
            auto path = finfo.symLinkTarget().toStdString();

            auto is_output = path.find("/sys/devices/virtual/") == 0;
            auto& target = is_output ? output : input;
            if (target.empty()) {
                target = "/dev/" + name;
            }
        }
    }

    if (input.empty()) {
        input = DEFAULT_VIDEO_INPUT;
    }

    if (output.empty()) {
        output = DEFAULT_VIDEO_OUTPUT;
    }
}

Entry::Entry(ImgOps* ops) :
    m_ops { ops },
    m_widget { new QWidget {} },
    m_symbol { new QLabel { ops->symbol().c_str() } },
    m_name { new QLabel { ops->name().c_str() } } {

    m_symbol->setAlignment(Qt::AlignCenter);
    m_symbol->setFixedWidth(20);

    auto layout = new QHBoxLayout {};
    layout->addWidget(m_symbol);
    layout->addWidget(m_name);
    layout->addStretch(1);
    m_layout = layout;

    m_widget->setLayout(layout);
    m_widget->adjustSize();
}

void Entry::add_button(const char* name, std::function<void(void)> click) {
    auto button = new QPushButton { name };
    QObject::connect(button, &QPushButton::clicked, click);
    m_layout->addWidget(button);
}

Gui::Gui(int& argc, char** argv) :
    m_application { new QApplication { argc, argv } },
    m_widget { new QListWidget {} } {

    QString help = "Path to %1 video device. Defaults to '%2'.";
    QCommandLineOption video_input(
            { "i", "input" },
            help.arg("input").arg(DEFAULT_VIDEO_INPUT),
            "FILE");
    QCommandLineOption video_output(
            { "o", "output" },
            help.arg("output").arg(DEFAULT_VIDEO_OUTPUT),
            "FILE");

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addOption(video_input);
    parser.addOption(video_output);
    parser.process(*m_application);

    if (parser.isSet(video_input)) {
        m_video_input = parser.value(video_input).toStdString();
    }

    if (parser.isSet(video_output)) {
        m_video_output = parser.value(video_output).toStdString();
    }

    default_devices(m_video_input, m_video_output);

    auto create_entry = [&](ImgOps* ops) {
        Entry entry { ops };

        auto index = new QListWidgetItem {};
        index->setSizeHint(entry.widget()->size());

        m_widget->addItem(index);
        m_widget->setItemWidget(index, entry.widget());

        auto onclick = [=](QListWidgetItem* item) {
            if (item == index) {
                std::lock_guard lock { g_mutex };
                m_current_op = ops;
            }
        };
        QObject::connect(m_widget, &QListWidget::itemClicked, onclick);

        if (m_widget->count() == 1) {
            std::lock_guard lock { g_mutex };
            m_current_op = ops;
            index->setSelected(true);
        }

        m_imgops.push_back(ops);
        return entry;
    };

    create_entry(new OpPassthrough {});

    create_entry(new OpHoldImage {});

    auto entry = create_entry(new OpSolidColor { 0, 0, 0 });
    entry.add_button("Select color", [entry]() {
        auto ops = static_cast<OpSolidColor*>(entry.ops());

        unsigned char r, g, b;
        ops->getColor(r, g, b);

        auto color = QColorDialog::getColor(QColor { r, g, b });
        if (!color.isValid()) {
            return;
        }

        auto palette = entry.symbol()->palette();
        palette.setColor(entry.symbol()->foregroundRole(), color);
        entry.symbol()->setPalette(palette);
        ops->setColor(color.red(), color.green(), color.blue());
    });

    auto basepath =
            QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

    QDir dir { basepath + QDir::separator() + "images" };
    dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

    for (const auto& finfo : dir.entryInfoList()) {
        auto path = finfo.absoluteFilePath().toStdString();
        create_entry(new OpStaticImage { path });
    }
}

Gui::~Gui() {
    for (const auto& imgop : m_imgops) {
        delete imgop;
    }

    delete m_widget;
    delete m_application;
}

bool Gui::terminated() {
    std::lock_guard lock { g_mutex };
    return m_terminated;
}

ImgOps* Gui::current_op() {
    std::lock_guard lock { g_mutex };
    return m_current_op;
}

int Gui::run() {
    m_widget->show();

    auto retval = m_application->exec();

    std::lock_guard lock { g_mutex };
    m_terminated = true;

    return retval;
}

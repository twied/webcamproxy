// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "imageops.h"

#include "canvas.h"

#include <QFileInfo>
#include <QImage>

#include <mutex>
#include <thread>

static std::mutex g_mutex {};

void OpPassthrough::fill(Canvas& canvas) {
    canvas.read_image();
}

void OpHoldImage::fill(class Canvas&) {
    /* nothing to do */
    std::this_thread::sleep_for(std::chrono::milliseconds { 100 });
}

OpSolidColor::OpSolidColor(unsigned char r, unsigned char g, unsigned char b) {
    setColor(r, g, b);
}

void OpSolidColor::getColor(unsigned char& r, unsigned char& g, unsigned char& b) {
    const std::lock_guard<std::mutex> lock { g_mutex };
    r = m_red;
    g = m_green;
    b = m_blue;
}

void OpSolidColor::setColor(unsigned char r, unsigned char g, unsigned char b) {
    const std::lock_guard<std::mutex> lock { g_mutex };
    m_red = r;
    m_green = g;
    m_blue = b;
}

void OpSolidColor::fill(class Canvas& canvas) {
    auto& data = canvas.data();

    unsigned char r, g, b;
    getColor(r, g, b);

    for (size_t i = 0; i < data.size(); i += 3) {
        data[i + 0] = r;
        data[i + 1] = g;
        data[i + 2] = b;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds { 100 });
}

OpStaticImage::OpStaticImage(const std::string& filename) {
    m_name = QFileInfo { filename.c_str() }.fileName().toStdString();

    auto image = new QImage { filename.c_str() };
    m_impl = image;

    for (int y = 0; !m_overlay && y < image->height(); ++y) {
        for (int x = 0; !m_overlay && x < image->width(); ++x) {
            m_overlay |= (0xff & (image->pixel(x, y) >> 24)) != 0xff;
        }
    }
}

void OpStaticImage::fill(class Canvas& canvas) {
    QImage* image = static_cast<QImage*>(m_impl);
    auto& data = canvas.data();

    if (m_overlay) {
        canvas.read_image();
    } else {
        std::this_thread::sleep_for(std::chrono::milliseconds { 100 });
    }

    for (int y = 0; y < std::min(image->height(), canvas.height()); ++y) {
        for (int x = 0; x < std::min(image->width(), canvas.width()); ++x) {
            auto overlay = image->pixel(x, y);
            auto a = 0xff & (overlay >> 24);
            auto r = 0xff & (overlay >> 16);
            auto g = 0xff & (overlay >> 8);
            auto b = 0xff & (overlay >> 0);

            size_t offset = (y * canvas.width() + x) * 3;
            data[offset + 0] = ((a * r) + data[offset + 0] * (255 - a)) / 255;
            data[offset + 1] = ((a * g) + data[offset + 1] * (255 - a)) / 255;
            data[offset + 2] = ((a * b) + data[offset + 2] * (255 - a)) / 255;
        }
    }
}

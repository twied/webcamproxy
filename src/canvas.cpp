// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "canvas.h"

#include <fcntl.h>
#include <libv4l2.h>
#include <linux/videodev2.h>

#include <string>
#include <system_error>

Canvas::Canvas(const std::string& input, const std::string& output) {
    if ((m_input = v4l2_open(input.data(), O_RDWR)) < 0) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot open input camera " + input
        };
    }

    if ((m_output = v4l2_open(output.data(), O_RDWR)) < 0) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot open output camera " + output
        };
    }

    v4l2_format format {};
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (v4l2_ioctl(m_input, VIDIOC_G_FMT, &format) != 0) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot get video format"
        };
    }

    m_width = format.fmt.pix.width;
    m_height = format.fmt.pix.height;

    format.fmt.pix = {
        m_width,
        m_height,
        V4L2_PIX_FMT_RGB24,
        V4L2_FIELD_ANY,
        3 * m_width,
        3 * m_width * m_height,
        V4L2_COLORSPACE_DEFAULT,
        0,
        0,
        { 0 },
        V4L2_QUANTIZATION_DEFAULT,
        V4L2_XFER_FUNC_DEFAULT
    };
    if (v4l2_ioctl(m_input, VIDIOC_S_FMT, &format) != 0) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot set input video format"
        };
    }

    format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    if (v4l2_ioctl(m_output, VIDIOC_S_FMT, &format) != 0) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot set output video format"
        };
    }

    m_data.resize(format.fmt.pix.sizeimage);
}

Canvas::~Canvas() {
    if (m_input >= 0) {
        v4l2_close(m_input);
    }

    if (m_output >= 0) {
        v4l2_close(m_output);
    }
}

void Canvas::read_image() {
    ssize_t n = v4l2_read(m_input, m_data.data(), m_data.size());

    if (n < 0 || static_cast<size_t>(n) != m_data.size()) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot read frame"
        };
    }
}

void Canvas::write_image() {
    ssize_t n = v4l2_write(m_output, m_data.data(), m_data.size());

    if (n < 0 || static_cast<size_t>(n) != m_data.size()) {
        throw std::system_error {
            errno,
            std::generic_category(),
            "cannot write frame"
        };
    }
}

void Canvas::enable_input(bool value) {
    if (m_enable_input == value) {
        return;
    }

    m_enable_input = value;

    if (value) {
        /* nothing to do; just read() */
    } else {
        int data { V4L2_BUF_TYPE_VIDEO_CAPTURE };
        if (v4l2_ioctl(m_input, VIDIOC_STREAMOFF, &data) != 0) {
            throw std::system_error {
                errno,
                std::generic_category(),
                "cannot disable stream"
            };
        }
    }
}
